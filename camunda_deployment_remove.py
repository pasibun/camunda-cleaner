
import json, sys
import urllib.request

url = "http://localhost:8095/engine-rest/engine/default/deployment/"
cascade_sub = "?cascade=true&skipCustomListeners=true&skipIoMappings=true"

def connecting_to_url():
	try:
		print("Deleting deployments from camunda.")
		json_url = urllib.request.urlopen(url)
		data = json.loads(json_url.read())
		ids = [item.get('id') for item in data]
	except urllib.error.HTTPError as e:
		print('Staat de pa-service in een breakpoint? HTTPError: {}'.format(e.code))
		exit_application()
	except urllib.error.URLError as e:
		print('Draait de pa service? URLError: {}'.format(e.reason))
		exit_application()
	else:
		return ids

def deleting_deployments(ids):
	for id in ids:
		try:
			print("Deleting deployment with id: " + id)
			req = urllib.request.Request(url+id+cascade_sub)
			req.get_method = lambda: 'DELETE'
			response = urllib.request.urlopen(req)
		except urllib.error.HTTPError as e:
			print('Staat de pa-service in een breakpoint? HTTPError: {}'.format(e.code))
			exit_application()
		except urllib.error.URLError as e:
			print('Draait de pa service? URLError: {}'.format(e.reason))		
			exit_application()
		else:
			print('Deployment deleted, ' + id)

def exit_application():
	print("Exit application")
	sys.exit()


if __name__ == "__main__":
    print("Camunda remove deployments")
    ids = connecting_to_url()
    deleting_deployments(ids)
    print("Deployments deleted.")
    exit_application()
